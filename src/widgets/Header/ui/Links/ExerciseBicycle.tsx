import { ExerciseBicycleImage } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface ExerciseBicycleProps {
	className?: string
}

const ExerciseBicycle = ({ className }: ExerciseBicycleProps) => {
	const isMenuOpen = exerciseBicycleStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		exerciseBicycleStore.closeMenu()
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)

	return (
		<Modal
			className={cn(cls.modal, [cls.fifty])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cn(cls.inner, [cls.pad])} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Аксессуары <br /> для велостанков
					</AppLink>
				</div>
				<Image
					className={cn(cls.image, [cls.bottomCImage])}
					src={ExerciseBicycleImage}
					alt="изображение велотренажера"
				/>
			</div>
		</Modal>
	)
}

export default ExerciseBicycle
