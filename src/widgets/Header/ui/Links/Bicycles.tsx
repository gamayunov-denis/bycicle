import { BicycleLink } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface BicyclesProps {
	className?: string
}

const Bicycles = ({ className }: BicyclesProps) => {
	const isMenuOpen = bicyclesStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		bicyclesStore.closeMenu()
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)
	return (
		<Modal
			className={cn(cls.modal, [cls.thirtyFive])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cn(cls.inner, [cls.pad])} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Горные велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Городские велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Шоссейные велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Гравийные велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Трековые велосипеды
					</AppLink>
				</div>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велосипеды для триатлона
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Двухподвесные велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Электровелосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Женские велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Детские велосипеды
					</AppLink>
				</div>
				<Image
					className={cn(cls.image, [cls.bottomCImage])}
					src={BicycleLink}
					alt="изображение велосипеда"
				/>
			</div>
		</Modal>
	)
}

export default Bicycles
