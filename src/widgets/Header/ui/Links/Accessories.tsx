import { AccessoriesImage } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface AccessoriesProps {
	className?: string
}

const Accessories = ({ className }: AccessoriesProps) => {
	const isMenuOpen = accessoriesStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)

	return (
		<Modal
			className={cn(cls.modal, [cls.fifty])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cls.inner} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велокомпьютеры
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Инструменты
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Вилки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велочехлы
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Крылья
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Насосы
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Фляги, держатели
					</AppLink>
				</div>
				<Image
					className={cn(cls.image, [cls.rightCImage])}
					src={AccessoriesImage}
					alt="изображение инструментов"
				/>
			</div>
		</Modal>
	)
}

export default Accessories
