import { Slam } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface TradeInProps {
	className?: string
}

const TradeIn = ({ className }: TradeInProps) => {
	const isMenuOpen = tradeInStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		tradeInStore.closeMenu()
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)

	return (
		<Modal
			className={cn(cls.modal, [cls.thirty])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cls.inner} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велосипеды
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Горные велосипеды
					</AppLink>
				</div>
				<Image className={cls.image} src={Slam} alt="изображение шлема" />
			</div>
		</Modal>
	)
}

export default TradeIn
