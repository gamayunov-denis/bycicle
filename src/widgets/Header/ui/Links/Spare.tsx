import { Wheels } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface SpareProps {
	className?: string
}

const Spare = ({ className }: SpareProps) => {
	const isMenuOpen = spareStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		spareStore.closeMenu()
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)

	return (
		<Modal
			className={cn(cls.modal, [cls.thirty])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cls.inner} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Группы оборудования
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Рамы
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Вилки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Запчасти для электронных групп
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Каретки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Камеры
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Кассеты
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Колёса
					</AppLink>
				</div>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Манетки/Шифтеры
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Педали
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Переключатели скоростей
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Покрышки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Сёдла и штыри
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Системы шатунов
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Тормоза
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Цепи
					</AppLink>
				</div>
				<Image className={cls.image} src={Wheels} alt="изображение колес" />
			</div>
		</Modal>
	)
}

export default Spare
