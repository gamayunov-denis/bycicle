import { Slam } from '../../../../../public/assets'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	headerMenuStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import Image from 'next/image'
import { useRef } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Modal } from '@/shared/ui/Modal/Modal'
import cls from './Links.module.scss'

interface OutfitProps {
	className?: string
}

const Outfit = ({ className }: OutfitProps) => {
	const isMenuOpen = outfitStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		outfitStore.closeMenu()
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)
	return (
		<Modal
			className={cn(cls.modal, [cls.fifty])}
			isMenuOpen={isMenuOpen}
			setIsMenuOpen={closeMenu}
			linkModal
		>
			<div className={cls.inner} ref={menuRef}>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велокуртки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велотрусы
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Термобелье
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велообувь
					</AppLink>
				</div>
				<div className={cls.items}>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велошлемы
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Велоперчатки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Очки
					</AppLink>
					<AppLink className={cls.link} appRoute={AppRoutes.HOME}>
						Аксессуары
					</AppLink>
				</div>
				<Image className={cls.image} src={Slam} alt="изображение шлема" />
			</div>
		</Modal>
	)
}

export default Outfit
