import headerMenuStore from '../../model/store/headerMenuStore'
import { observer } from 'mobx-react-lite'
import { useRef } from 'react'
import { TfiClose } from 'react-icons/tfi'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Portal } from '@/shared/ui/Portal/Portal'
import cls from './HeaderMenu.module.scss'

interface HeaderMenuProps {
	className?: string
}

const HeaderMenu = ({ className }: HeaderMenuProps) => {
	const isMenuOpen = headerMenuStore.isMenuOpen

	const menuRef = useRef(null)

	const closeMenu = () => {
		headerMenuStore.closeMenu()
	}

	useMenuClose(isMenuOpen, closeMenu, menuRef)

	return (
		<Portal>
			<div
				ref={menuRef}
				className={cn(cls.HeaderMenu, [className, isMenuOpen && cls.menuOpen])}
			>
				<TfiClose className={cls.close} onClick={closeMenu} />
				<h4 className={cls.title}>Доп. Информация</h4>
				<AppLink className={cls.link} appRoute={AppRoutes.ABOUT_US}>
					О нас
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.REPAIR}>
					Веломастерская
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.KEEPING}>
					Хранение
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.GUARANTEE}>
					Гарантии
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.AGREEMENT}>
					Пользовательское соглашение
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.DELIVERY}>
					Доставка и оплата
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.BLOG}>
					Блог
				</AppLink>
				<AppLink className={cls.link} appRoute={AppRoutes.CONTACTS}>
					Контакты
				</AppLink>
			</div>
		</Portal>
	)
}

export default observer(HeaderMenu)
