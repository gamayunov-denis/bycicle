'use client'

import {
	BicycleShop,
	Cart,
	Heart,
	Menu,
	Search,
	User,
} from '../../../../../public/assets'
import headerMenuStore from '../../model/store/headerMenuStore'
import {
	accessoriesStore,
	bicyclesStore,
	exerciseBicycleStore,
	outfitStore,
	spareStore,
	tradeInStore,
} from '../../model/store/linkStore'
import HeaderMenu from '../HeaderMenu/HeaderMenu'
import Accessories from '../Links/Accessories'
import Bicycles from '../Links/Bicycles'
import ExerciseBicycle from '../Links/ExerciseBicycle'
import Outfit from '../Links/Outfit'
import Spare from '../Links/Spare'
import TradeIn from '../Links/TradeIn'
import Image from 'next/image'
import { observer } from 'mobx-react-lite'
import { useEffect } from 'react'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import { cn } from '@/shared/lib/classNames/cn'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import cls from './Header.module.scss'

interface HeaderProps {
	className?: string
}

const Header = ({ className }: HeaderProps) => {
	useEffect(() => {
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
	}, [])

	const handleLink = (fn: () => void) => () => {
		accessoriesStore.closeMenu()
		bicyclesStore.closeMenu()
		exerciseBicycleStore.closeMenu()
		headerMenuStore.closeMenu()
		outfitStore.closeMenu()
		spareStore.closeMenu()
		tradeInStore.closeMenu()
		fn()
	}

	return (
		<header className={cn(cls.Header, [className])}>
			<AppLink appRoute={AppRoutes.HOME}>
				<Image
					src={BicycleShop}
					alt="bicycle"
					className={cls.logo}
					width={50}
					height={50}
				/>
			</AppLink>
			<nav className={cls.nav}>
				<div
					className={cls.navItem}
					onClick={handleLink(tradeInStore.toggleMenu)}
				>
					Trade In {tradeInStore.isMenuOpen && <TradeIn />}
				</div>
				<div
					className={cls.navItem}
					onClick={handleLink(bicyclesStore.toggleMenu)}
				>
					Велосипеды {bicyclesStore.isMenuOpen && <Bicycles />}
				</div>
				<div
					className={cls.navItem}
					onClick={handleLink(spareStore.toggleMenu)}
				>
					Запчасти {spareStore.isMenuOpen && <Spare />}
				</div>
				<div
					onClick={handleLink(outfitStore.toggleMenu)}
					className={cls.navItem}
				>
					Экипировка {outfitStore.isMenuOpen && <Outfit />}
				</div>
				<div
					onClick={handleLink(accessoriesStore.toggleMenu)}
					className={cls.navItem}
				>
					Аксессуары {accessoriesStore.isMenuOpen && <Accessories />}
				</div>
				<div
					onClick={handleLink(exerciseBicycleStore.toggleMenu)}
					className={cls.navItem}
				>
					Велостанки {exerciseBicycleStore.isMenuOpen && <ExerciseBicycle />}
				</div>
			</nav>
			<menu className={cls.menu}>
				{/*<Input />*/}
				<Image
					src={Search}
					alt="Поиск"
					className={cls.navIcon}
					width={20}
					height={20}
				/>
				<AppLink appRoute={AppRoutes.PROFILE}>
					<Image
						src={User}
						alt="Профиль"
						className={cls.navIcon}
						width={20}
						height={20}
					/>
				</AppLink>
				<AppLink appRoute={AppRoutes.FAVORITES}>
					<Image
						src={Heart}
						alt="Понравилось"
						className={cls.navIcon}
						width={20}
						height={20}
					/>
				</AppLink>
				<AppLink appRoute={AppRoutes.CART}>
					<Image
						src={Cart}
						alt="Корзина"
						className={cls.navIcon}
						width={20}
						height={20}
					/>
				</AppLink>
				<Image
					className={cn(cls.navIcon, [cls.menuBtn])}
					src={Menu}
					alt="Меню"
					width={20}
					height={20}
					onClick={headerMenuStore.toggleMenu}
				/>
			</menu>
			{headerMenuStore.isMenuOpen && <HeaderMenu />}
		</header>
	)
}

export default observer(Header)
