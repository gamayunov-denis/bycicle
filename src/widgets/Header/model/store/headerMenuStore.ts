import { action, makeAutoObservable } from 'mobx'

class HeaderMenuStore {
	isMenuOpen = false

	constructor() {
		makeAutoObservable(this, {
			toggleMenu: action.bound,
			closeMenu: action,
		})
	}

	toggleMenu() {
		this.isMenuOpen = !this.isMenuOpen
	}

	closeMenu() {
		this.isMenuOpen = false
	}
}

const headerMenuStore = new HeaderMenuStore()

export default headerMenuStore
