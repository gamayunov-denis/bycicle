import { action, makeAutoObservable } from 'mobx'

class LinkStore {
	isMenuOpen = false

	constructor() {
		makeAutoObservable(this, {
			toggleMenu: action.bound,
			closeMenu: action,
		})
	}

	toggleMenu() {
		this.isMenuOpen = !this.isMenuOpen
	}

	closeMenu() {
		this.isMenuOpen = false
	}
}

export const accessoriesStore = new LinkStore()
export const bicyclesStore = new LinkStore()
export const exerciseBicycleStore = new LinkStore()
export const headerMenuStore = new LinkStore()
export const outfitStore = new LinkStore()
export const spareStore = new LinkStore()
export const tradeInStore = new LinkStore()
