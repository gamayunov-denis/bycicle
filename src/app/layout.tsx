import type { Metadata } from 'next'
import { Oswald } from 'next/font/google'
import { ReactNode, Suspense } from 'react'
import { Header } from '@/widgets/Header'
import './styles/global.scss'

const font = Oswald({
	weight: ['200', '300', '400', '500', '700'],
	subsets: ['cyrillic'],
})

export const metadata: Metadata = {
	title: 'Bicycle',
	description: 'Bicycle app',
	icons: {
		icon: ['/favicon.ico?v=1'],
		apple: ['/apple-touch-icon.png?v=4'],
		shortcut: ['/apple-touch-icon.png'],
	},
	manifest: '/site.web-manifest',
}

export default function RootLayout({ children }: { children: ReactNode }) {
	return (
		<html lang="ru">
			<body className={font.className}>
				<Header />
				{children}
			</body>
		</html>
	)
}
