import NotFoundPage from "@/shared/ui/NotFoundPage/NotFoundPage";

export default async function NotFound() {
	return <NotFoundPage />
}
