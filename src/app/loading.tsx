import { Loader } from '../../public/assets'
import Image from 'next/image'

export default function Loading() {
	return (
		<div className="loaderInner">
			<Image
				className="loader"
				width={60}
				height={60}
				src={Loader}
				alt="Загрузка..."
			/>
		</div>
	)
}
