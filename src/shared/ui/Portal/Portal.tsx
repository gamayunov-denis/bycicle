import { ReactNode, useEffect, useState } from 'react'
import { createPortal } from 'react-dom'

interface PortalProps {
	children: ReactNode
	element?: HTMLElement
}

export const Portal = (props: PortalProps) => {
	const { children, element = document.body } = props
	const [mounted, setMounted] = useState(false)

	useEffect(() => {
		setMounted(true)
	}, [])

	if (!mounted || typeof window === 'undefined') {
		return null
	}

	return createPortal(children, element)
}
