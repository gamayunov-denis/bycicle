'use client'

import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { ReactNode } from 'react'
import { withErrorBoundary } from 'react-error-boundary'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import { cn } from '@/shared/lib/classNames/cn'
import ErrorFallback from '@/shared/ui/ErrorFallback/ErrorFallback'
import cls from './AppLink.module.scss'

interface customLinkProps {
	className?: string
	children?: ReactNode
	appRoute: AppRoutes
}

export const AppLink = ({ className, children, appRoute }: customLinkProps) => {
	const pathname = usePathname()

	return (
		<Link
			className={cn('', [
				className,
				pathname === appRoute ? cls.disableEvents : '',
			])}
			href={appRoute}
		>
			{children}
		</Link>
	)
}

export default withErrorBoundary(AppLink, { fallback: <ErrorFallback /> })
