import { text } from 'stream/consumers'
import { ChangeEvent, InputHTMLAttributes } from 'react'
import { cn } from '@/shared/lib/classNames/cn'
import cls from './Input.module.scss'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
	className?: string
	type?: string
	value?: string
	onChange?: (e: ChangeEvent<HTMLInputElement>) => void
	pattern?: string
	isValid?: boolean
}

export const Input = ({
	className,
	type = 'text',
	isValid = true,
	onChange,
	value,
	pattern,
	...otherProps
}: InputProps) => {
	const validateInput = isValid ? '' : cls.invalidInput

	return (
		<input
			className={cn(cls.Input, [className, validateInput])}
			type={type}
			value={value}
			onChange={onChange}
			pattern={pattern}
		/>
	)
}
