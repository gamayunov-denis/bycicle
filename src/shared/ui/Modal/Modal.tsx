import { ReactNode, useCallback, useEffect, useRef } from 'react'
import useMenuClose from '@/shared/hooks/useMenuClose'
import { cn } from '@/shared/lib/classNames/cn'
import { Portal } from '@/shared/ui/Portal/Portal'
import cls from './Modal.module.scss'

interface ModalProps {
	className?: string
	children?: ReactNode
	linkModal?: boolean
	center?: boolean
	isMenuOpen?: boolean
	setIsMenuOpen?: () => void
}

export const Modal = (props: ModalProps) => {
	const {
		className,
		children,
		linkModal,
		center,
		isMenuOpen = false,
		setIsMenuOpen,
	} = props
	const modalRef = useRef(null)

	const closeMenu = useCallback(() => {
		if (setIsMenuOpen) {
			setIsMenuOpen()
		}
	}, [setIsMenuOpen])

	useEffect(() => {
		const handleScroll = () => {
			closeMenu()
		}
		window.addEventListener('scroll', handleScroll)

		return () => {
			window.removeEventListener('scroll', handleScroll)
		}
	}, [closeMenu])

	useMenuClose(isMenuOpen, closeMenu, modalRef)

	return (
		<Portal>
			<div
				ref={modalRef}
				className={cn('', [
					className,
					linkModal ? cls.linkModal : '',
					center ? cls.Modal : '',
				])}
			>
				{children}
			</div>
		</Portal>
	)
}
