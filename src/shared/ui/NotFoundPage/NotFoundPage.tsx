import { AppRoutes } from '@/shared/config/routes/appRoutes'
import { AppLink } from '@/shared/ui/AppLink/AppLink'
import { Button } from '@/shared/ui/Button/Button'
import cls from './NotFoundPage.module.scss'

const NotFoundPage = () => {
	return (
		<div className={cls.notFound}>
			<h1 className={cls.title}>404</h1>
			<p>Сожалеем, но данной страницы не существует</p>
			<AppLink appRoute={AppRoutes.HOME}>
				<Button className={cls.btn}>Вернуться</Button>
			</AppLink>
		</div>
	)
}

export default NotFoundPage
