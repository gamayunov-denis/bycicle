'use client'

import Link from 'next/link'
import { AppRoutes } from '@/shared/config/routes/appRoutes'
import { Button } from '@/shared/ui/Button/Button'
import cls from './ErrorFallback.module.scss'

const ErrorFallback = ({ error, resetErrorBoundary }) => {
	return (
		<section className={cls.inner}>
			<h1>Что-то пошло не так...</h1>
			<pre>{error.message}</pre>
			<h4>Попробуйте перезагрузить страницу,</h4>
			<Link className={cls.link} href="https://www.gamayunov.website/">
				или обратитесь к разработчикам
			</Link>
			<Link href={AppRoutes.HOME}>
				<Button onClick={resetErrorBoundary} className={cls.btn}>
					Попробовать снова
				</Button>
			</Link>
		</section>
	)
}

export default ErrorFallback
