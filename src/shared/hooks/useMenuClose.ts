import { RefObject, useCallback, useEffect } from 'react'

function useMenuClose(
	menuOpen: boolean,
	closeMenu: () => void,
	menuRef: RefObject<HTMLElement>
) {
	const handleKeysPress = useCallback(
		(e: KeyboardEvent) => {
			if (e.key === 'Escape') {
				closeMenu()
			}
		},
		[closeMenu]
	)

	const handleClickOutside = useCallback(
		(e: MouseEvent) => {
			if (menuRef.current && !menuRef.current.contains(e.target as Node)) {
				closeMenu()
			}
		},
		[closeMenu, menuRef]
	)

	useEffect(() => {
		if (menuOpen) {
			document.addEventListener('keydown', handleKeysPress)
			document.addEventListener('click', handleClickOutside)
		} else {
			document.removeEventListener('keydown', handleKeysPress)
			document.removeEventListener('click', handleClickOutside)
		}

		return () => {
			document.removeEventListener('keydown', handleKeysPress)
			document.removeEventListener('click', handleClickOutside)
		}
	}, [menuOpen, handleKeysPress, handleClickOutside])
}

export default useMenuClose
