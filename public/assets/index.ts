import Loader from './gif/loader.gif'
import AccessoriesImage from './png/accessories.png'
import BicycleLink from './png/bicycle-link.png'
import BicycleShop from './png/bicycle-shop.png'
import Bicycle from './png/bicycle.png'
import ExerciseBicycleImage from './png/exercise-bicycle.png'
import Logo from './png/logo.png'
import Slam from './png/slam.png'
import Wheels from './png/wheels.png'
import Cart from './svg/cart.svg'
import Heart from './svg/heart.svg'
import Menu from './svg/menu.svg'
import Search from './svg/search.svg'
import User from './svg/user.svg'

export {
	Logo,
	Cart,
	Heart,
	Search,
	User,
	Menu,
	Bicycle,
	BicycleShop,
	Loader,
	Slam,
	BicycleLink,
	Wheels,
	AccessoriesImage,
	ExerciseBicycleImage,
}
